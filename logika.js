function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open('GET',yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}

function loadJSON(url, callback) {   

    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType('application/json');
    xobj.open('GET', url, true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == '200') {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
          }
    };
    xobj.send(null);  
 }
 
 if(window && window.location && window.location.search) {
	var f = window.location.search.replace('?p=','');
	if(f) {
		loadJSON(f+'.json', function(odp){
			var dane = JSON.parse(odp);
			var html = '';
			
			for(var i = 0; i < dane.grupy.length; i++) {
				var grupa = dane.grupy[i];
				html += addStartDiv(addFullDiv(grupa.nazwa));
				
				for(var j = 0; j < grupa.osoby.length; j++) {
					var osoba = grupa.osoby[j];
					html += addFullDiv(' ' + osoba.imie + ':  ' + getDate(osoba.data));
				}
				
				html = addEndDiv(html);
				html = addBr(html);
			}
			
			document.getElementById('main').innerHTML = html;
		});
	}
 }
 
 
 const _MS_PER_DAY = 1000 * 60 * 60 * 24;
 function getDate(date) {
	 var d = new Date(date);
	 var days = Math.floor((new Date() - d) / _MS_PER_DAY);
	 
	 var years = Math.floor(days / 365);
	 var daysModulo = days % 365;
	 var months = Math.floor(daysModulo / 30);
	 var days = daysModulo % 30;
	 
	 return years.toString() + ' (+ ' + months.toString() + ' ms i ' + days.toString() + ' d)';
 }
 
 function addBr(content) { return content + '<br>';}
 function addFullDiv(content){ return '<div>' + content + '</div>';}
 function addStartDiv(content){ return '<div>' + content;}
 function addEndDiv(content){ return content + '</div>';}

// var dane = JSON.parse(Get("dane.json"));
